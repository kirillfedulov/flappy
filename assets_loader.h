
struct TarHeader {
	char Name[100];
	char Garbage1[24];
	char Size[12];
	char Garbage2[12];
	char Checksum[8];
	char Type;
	char Garbage3[100];
	char Magic[6];
	char Garbage4[249];
};

enum {
	TYPE_REGULAR_FILE = '0'
};

const int TarHeaderSize = sizeof(TarHeader);

static_assert(TarHeaderSize == 512);

static int GetSize(char SizeStr[12]) {
	int Size = 0;
	for (int J = 0; J < 12 - 1; J++) {
		Size *= 8;
		Size += SizeStr[J] - '0';
	}

	return Size;
}

struct TarFileIterator {
	unsigned char *TarStart;
	int TarSize;

	unsigned char *File;
	int FileSize;

	const char *FilePath;
};

TarFileIterator TarFirstFile(unsigned char *TarStart, int TarSize) {
	TarFileIterator Iterator = {0};
	Iterator.TarStart = TarStart;
	Iterator.TarSize = TarSize;

	for (TarHeader *Header = (TarHeader *) TarStart; (unsigned char *) Header < TarStart + TarSize; Header++) {
		if (Header->Type == TYPE_REGULAR_FILE) {
			Iterator.File = (unsigned char *) Header + TarHeaderSize;
			Iterator.FileSize = GetSize(Header->Size);
			Iterator.FilePath = Header->Name;
			break;
		}
	}

	return Iterator;
}

TarFileIterator TarNextFile(TarFileIterator Iterator) {
	TarFileIterator NewIterator = {0};
	NewIterator.TarStart = Iterator.TarStart;
	NewIterator.TarSize = Iterator.TarSize;

	TarHeader *Header = (TarHeader *) (Iterator.File + TarHeaderSize * (Iterator.FileSize / TarHeaderSize + 1));
	for (; (unsigned char *) Header < Iterator.TarStart + Iterator.TarSize; Header++) {
		if (Header->Type == TYPE_REGULAR_FILE) {
			NewIterator.File = (unsigned char *) Header + TarHeaderSize;
			NewIterator.FilePath = Header->Name;
			NewIterator.FileSize = GetSize(Header->Size);
			break;
		}
	}

	return NewIterator;
}

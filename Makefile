LIBS=sdl2 SDL2_image

CC=gcc
CFLAGS=-Wall -O3 -ggdb $(shell pkg-config --cflags ${LIBS})
LDFLAGS=-lm $(shell pkg-config --libs ${LIBS})

flappy: flappy.cpp assets_loader.h assets.tar.h
	${CC} ${CFLAGS} -o $@ $< ${LDFLAGS}

assets.tar.h:
	tar cf assets.tar assets/
	xxd -i assets.tar > assets.tar.h

clean:
	rm flappy assets.tar assets.tar.h

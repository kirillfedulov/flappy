#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <math.h>
#include <assert.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "assets.tar.h"
#include "assets_loader.h"

enum GameState {
	STATE_STARTING,
	STATE_PLAYING,
	STATE_DIED
};

struct LoadedTexture {
	SDL_Texture *Texture;
	int Width, Height;
};

struct LoadedAudio {
	uint8_t *Buffer;
	uint32_t Length;
	SDL_AudioDeviceID Device;
};

enum AssetType {
	ASSET_UNKNOWN,
	ASSET_TEXTURE,
	ASSET_AUDIO
};

struct LoadedAsset {
	AssetType Type;

	union {
		LoadedTexture Texture;
		LoadedAudio Audio;
	};
};

struct Pair {
	const char *Path;
	LoadedAsset Asset;

	bool Occupied;
};

const int MaxAssets = 256;
struct LoadedAssetsTable {
	int Size;
	Pair Assets[MaxAssets] = {};
} AssetsTable;


unsigned
HashString(const char *String) {
	unsigned Result = 5381;
	for (int I = 0; String[I]; I++) {
		Result = ((Result << 5) + Result) + String[I];
	}

	return Result;
}

LoadedAsset *
GetAsset(const char *Path) {
	int Index = HashString(Path) % MaxAssets;
	if (strcmp(AssetsTable.Assets[Index].Path, Path) == 0) {
		return &AssetsTable.Assets[Index].Asset;
	}

	for (int I = Index + 1; I != Index; I = (I + 1) % MaxAssets) {
		if (AssetsTable.Assets[I].Occupied && strcmp(AssetsTable.Assets[I].Path, Path) == 0) {
			return &AssetsTable.Assets[I].Asset;
		}
	}

	assert(0);
	return NULL;
}

LoadedTexture
LoadTexture(SDL_Renderer *Renderer, unsigned char *Memory, int MemorySize) {
	LoadedTexture Texture = {};

	SDL_RWops *RW = SDL_RWFromConstMem(Memory, MemorySize);
	if (!RW) {
		fprintf(stderr, "SDL_RWFromConstMem failed: %s\n", SDL_GetError());
		exit(-1);
	}

	Texture.Texture = IMG_LoadTexture_RW(Renderer, RW, 1);
	if (!Texture.Texture) {
		fprintf(stderr, "IMG_LoadTexture_RW failed: %s\n", SDL_GetError());
		exit(-1);
	}

	if (SDL_QueryTexture(Texture.Texture, NULL, NULL, &Texture.Width, &Texture.Height) != 0) {
		fprintf(stderr, "SDL_QueryTexture failed: %s\n", SDL_GetError());
		exit(-1);
	}

	return Texture;
}

LoadedAudio
LoadAudio(SDL_AudioSpec *Format, unsigned char *Memory, int MemorySize) {
	LoadedAudio Audio = {};

	SDL_RWops *RW = SDL_RWFromConstMem(Memory, MemorySize);
	if (!RW) {
		fprintf(stderr, "SDL_RWFromConstMem failed: %s", SDL_GetError());
		exit(-1);
	}

	if (SDL_LoadWAV_RW(RW, 1, Format, &Audio.Buffer, &Audio.Length) == NULL) {
		fprintf(stderr, "SDL_LoadWAV failed: %s", SDL_GetError());
		exit(-1);
	}

	Audio.Device = SDL_OpenAudioDevice(NULL, 0, Format, NULL, SDL_AUDIO_ALLOW_ANY_CHANGE);
	if (Audio.Device < 0) {
		fprintf(stderr, "SDL_OpenAudioDevice failed: %s", SDL_GetError());
		exit(-1);
	}

	SDL_PauseAudioDevice(Audio.Device, 0);

	return Audio;
}

void
PlayAudio(LoadedAudio *Audio) {
	SDL_QueueAudio(Audio->Device, Audio->Buffer, Audio->Length);
}

void
LoadAsset(SDL_Renderer *Renderer, SDL_AudioSpec *AudioFormat, const char *Path, unsigned char *Memory, int MemorySize) {
	LoadedAsset Asset = {};

	int Length = strlen(Path);
	if (strncmp(Path + Length - 4, ".png", 4) == 0) {
		Asset.Type = ASSET_TEXTURE;
		Asset.Texture = LoadTexture(Renderer, Memory, MemorySize);
	} else if (strncmp(Path + Length - 4, ".wav", 4) == 0) {
		Asset.Type = ASSET_AUDIO;
		Asset.Audio = LoadAudio(AudioFormat, Memory, MemorySize);
	} else {
		assert(0);
	}

	int Index = HashString(Path) % MaxAssets;
	if (!AssetsTable.Assets[Index].Occupied) {
		AssetsTable.Assets[Index].Path = Path;
		AssetsTable.Assets[Index].Asset = Asset;
		AssetsTable.Assets[Index].Occupied = true;
	} else {
		for (int I = Index + 1; I != Index; I = (I + 1) % MaxAssets) {
			if (!AssetsTable.Assets[I].Occupied) {
				AssetsTable.Assets[I].Path = Path;
				AssetsTable.Assets[I].Asset = Asset;
				AssetsTable.Assets[I].Occupied = true;
				break;
			}
		}
	}
}

void
RenderBackground(SDL_Renderer *Renderer, LoadedTexture *Texture, int Scroll, int WindowWidth, int WindowHeight) {
	for (int I = 0; I < WindowWidth / Texture->Width + 2; I++) {
		SDL_Rect Source = {0, 0, Texture->Width, Texture->Height};
		SDL_Rect Destination = {Scroll + Texture->Width * I, 0, Source.w, WindowHeight};
		SDL_RenderCopy(Renderer, Texture->Texture, &Source, &Destination);
	}
}

struct Portal {
	int X, YTop, YBottom;
	bool ScrolledAway;
};

void
RenderPipes(SDL_Renderer *Renderer, LoadedTexture *Texture, Portal *Portals, int PortalsCount, int WindowHeight) {
	for (int I = 0; I < PortalsCount; I++) {
		if (!Portals[I].ScrolledAway) {
			const int PipeTopPixels = 50;

			SDL_Rect Source = {0, 0, Texture->Width, Texture->Height};

			SDL_Rect TopDestination = {Portals[I].X, Portals[I].YTop - Source.h, Source.w, Source.h};
			if (TopDestination.y > 0) {
				SDL_Rect FillerSource = Source;
				FillerSource.y = PipeTopPixels;

				for (int J = 0; J < TopDestination.y / FillerSource.h + 1; J++) {
					SDL_Rect FillerDestination = TopDestination;
					FillerDestination.y -= FillerSource.h * (J + 1);
					SDL_RenderCopy(Renderer, Texture->Texture, &FillerSource, &FillerDestination);
				}
			}
			SDL_RenderCopyEx(Renderer, Texture->Texture, &Source, &TopDestination, 0.0, NULL, SDL_FLIP_VERTICAL);

			SDL_Rect BottomDestination = {Portals[I].X, Portals[I].YBottom, Source.w, Source.h};
			if (WindowHeight - BottomDestination.y > Source.h) {
				SDL_Rect FillerSource = Source;
				FillerSource.y = PipeTopPixels;

				for (int J = 0; J < (WindowHeight - (BottomDestination.y + FillerSource.h)) / FillerSource.h + 1; J++) {
					SDL_Rect FillerDestination = BottomDestination;
					FillerDestination.y += FillerSource.h * (J + 1);
					SDL_RenderCopyEx(Renderer, Texture->Texture, &FillerSource, &FillerDestination, 0.0, NULL, SDL_FLIP_VERTICAL);
				}
			}
			SDL_RenderCopy(Renderer, Texture->Texture, &Source, &BottomDestination);
		}
	}
}

void
RenderForeground(SDL_Renderer *Renderer, LoadedTexture *Texture, int WindowWidth, int WindowHeight) {
	for (int I = 0; I < WindowWidth / Texture->Width + 1; I++) {
		SDL_Rect Source = {0, 0, Texture->Width, Texture->Height};
		SDL_Rect Destination = {Source.w * I, WindowHeight - Source.h, Source.w, Source.h};
		SDL_RenderCopy(Renderer, Texture->Texture, &Source, &Destination);
	}
}

void
RenderBird(SDL_Renderer *Renderer, LoadedTexture *Texture, int BirdX, int BirdY, float Angle) {
	SDL_Rect Source = {0, 0, Texture->Width, Texture->Height};
	SDL_Rect Destination = {BirdX, BirdY, Source.w, Source.h};
	int AnimationIndex = 1;
	if (Angle < -5.0f) {
		AnimationIndex = 0;
	} else if (Angle > 10.0f) {
		AnimationIndex = 2;
	}

	SDL_RenderCopyEx(Renderer, (Texture + AnimationIndex)->Texture, &Source, &Destination, Angle, NULL, SDL_FLIP_NONE);
}

void
RenderScore(SDL_Renderer *Renderer, LoadedTexture *ScoreTextures, int Score, int ScoreX, int ScoreY) {
	for (int I = 0, N = Score; N || I == 0; I++, N /= 10) {
		LoadedTexture *Texture = ScoreTextures + (N % 10);

		SDL_Rect Source = {0, 0, Texture->Width, Texture->Height};
		SDL_Rect Destination = {ScoreX - Source.w * I, ScoreY, Source.w, Source.h};
		SDL_RenderCopy(Renderer, Texture->Texture, &Source, &Destination);
	}
}

int
main(void) {
	srand(time(NULL));

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		fprintf(stderr, "SDL_Init failed: %s\n", SDL_GetError());
		exit(-1);
	}

	int WindowWidth = 1280, WindowHeight = 720;
	SDL_Window *Window = SDL_CreateWindow("Flappy", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WindowWidth, WindowHeight, 0);
	if (!Window) {
		fprintf(stderr, "SDL_CreateWindow failed: %s\n", SDL_GetError());
		exit(-1);
	}

	SDL_Renderer *Renderer = SDL_CreateRenderer(Window, -1, 0);
	if (!Renderer) {
		fprintf(stderr, "SDL_CreateRenderer failed: %s\n", SDL_GetError());
		exit(-1);
	}

	SDL_AudioSpec WavSpec = {0};
	WavSpec.freq = 44100;
	WavSpec.format = AUDIO_S16LSB;
	WavSpec.channels = 2;
	WavSpec.samples = 4096;

	for (TarFileIterator Iter = TarFirstFile(assets_tar, assets_tar_len); Iter.File; Iter = TarNextFile(Iter)) {
		LoadAsset(Renderer, &WavSpec, Iter.FilePath, Iter.File, Iter.FileSize);
	}

	const int BackgroundTexturesCount = 2;
	LoadedTexture BackgroundTextures[BackgroundTexturesCount] = {0};
	BackgroundTextures[0] = GetAsset("assets/sprites/background-day.png")->Texture;
	BackgroundTextures[1] = GetAsset("assets/sprites/background-night.png")->Texture;

	const int BirdAnimationFrames = 3;
	const int BirdTexturesCount = BirdAnimationFrames * 3;
	LoadedTexture BirdTextures[BirdTexturesCount] = {0};
	BirdTextures[0] = GetAsset("assets/sprites/bluebird-downflap.png")->Texture;
	BirdTextures[1] = GetAsset("assets/sprites/bluebird-midflap.png")->Texture;
	BirdTextures[2] = GetAsset("assets/sprites/bluebird-upflap.png")->Texture;
	BirdTextures[3] = GetAsset("assets/sprites/redbird-downflap.png")->Texture;
	BirdTextures[4] = GetAsset("assets/sprites/redbird-midflap.png")->Texture;
	BirdTextures[5] = GetAsset("assets/sprites/redbird-upflap.png")->Texture;
	BirdTextures[6] = GetAsset("assets/sprites/yellowbird-downflap.png")->Texture;
	BirdTextures[7] = GetAsset("assets/sprites/yellowbird-midflap.png")->Texture;
	BirdTextures[8] = GetAsset("assets/sprites/yellowbird-upflap.png")->Texture;

	const int PipeTexturesCount = 2;
	LoadedTexture PipeTextures[PipeTexturesCount] = {0};
	PipeTextures[0] = GetAsset("assets/sprites/pipe-green.png")->Texture;
	PipeTextures[1] = GetAsset("assets/sprites/pipe-red.png")->Texture;

	const int ScoreTexturesCount = 10;
	LoadedTexture ScoreTextures[ScoreTexturesCount] = {0};
	ScoreTextures[0] = GetAsset("assets/sprites/0.png")->Texture;
	ScoreTextures[1] = GetAsset("assets/sprites/1.png")->Texture;
	ScoreTextures[2] = GetAsset("assets/sprites/2.png")->Texture;
	ScoreTextures[3] = GetAsset("assets/sprites/3.png")->Texture;
	ScoreTextures[4] = GetAsset("assets/sprites/4.png")->Texture;
	ScoreTextures[5] = GetAsset("assets/sprites/5.png")->Texture;
	ScoreTextures[6] = GetAsset("assets/sprites/6.png")->Texture;
	ScoreTextures[7] = GetAsset("assets/sprites/7.png")->Texture;
	ScoreTextures[8] = GetAsset("assets/sprites/8.png")->Texture;
	ScoreTextures[9] = GetAsset("assets/sprites/9.png")->Texture;

	LoadedTexture ForegroundTexture = {0};
	ForegroundTexture = GetAsset("assets/sprites/base.png")->Texture;

	LoadedTexture GameoverTexture = {0};
	GameoverTexture = GetAsset("assets/sprites/gameover.png")->Texture;

	LoadedAudio PointAudio = GetAsset("assets/audio/point.wav")->Audio;
	LoadedAudio WingAudio = GetAsset("assets/audio/wing.wav")->Audio;
	LoadedAudio DieAudio = GetAsset("assets/audio/die.wav")->Audio;
	LoadedAudio HitAudio = GetAsset("assets/audio/hit.wav")->Audio;
	LoadedAudio SwooshAudio = GetAsset("assets/audio/swoosh.wav")->Audio;

	bool Running = true;

	int BackgroundTextureIndex = 0;
	int BirdTextureIndex = 0;
	int PipeTextureIndex = 0;

	int BackgroundScroll = 0;
	int BackgroundScrollSpeed = 1;

	uint64_t CurrFrameTicks = SDL_GetPerformanceCounter();
	uint64_t PrevFrameTicks = 0;
	float DeltaTime = 0.0f;
	float Time = 0.0f;
	int Frames = 0;

	const int DesiredFps = 120;
	const float DesiredDelta = 1.0f / DesiredFps;

	bool SpaceDown = false;
	bool SpacePressed = false;

	const int BirdX = 100;
	int BirdY = WindowHeight / 2;
	int BirdVelY = 0;
	const int Gravitation = 15;
	const int JumpVelocity = -WindowHeight;

	const int PortalsCount = 8;
	Portal Portals[PortalsCount] = {0};
	const int PortalsXStart = WindowWidth / 2;
	const int PortalsXMargin = 300;
	const int PortalHeight = 175;
	int PortalsScrollSpeed = 2;

	int Score = 0;
	const int ScoreX = WindowWidth / 2;
	const int ScoreY = 40;

	bool PassedPortal;

	GameState State = STATE_STARTING;

outer:  while (Running) {
		SDL_Event Event = {0};
		while (SDL_PollEvent(&Event)) {
			if (Event.type == SDL_QUIT) {
				Running = false;
				goto outer;
			}
		}

		bool SpaceWasDown = SpaceDown;
		SpaceDown = SDL_GetKeyboardState(NULL)[SDL_GetScancodeFromKey(SDLK_SPACE)];
		SpacePressed = !SpaceWasDown && SpaceDown;

		LoadedTexture *BackgroundTexture = BackgroundTextures + BackgroundTextureIndex;
		LoadedTexture *BirdTexture = BirdTextures + BirdTextureIndex;
		LoadedTexture *PipeTexture = PipeTextures + PipeTextureIndex;

		switch (State) {
		case STATE_STARTING: {
			BackgroundScrollSpeed = 1;
			BirdY = WindowHeight / 2;
			BirdVelY = JumpVelocity;
			PortalsScrollSpeed = 2;

			for (int I = 0; I < PortalsCount; I++) {
				Portals[I].X = PortalsXStart + PortalsXMargin * I;
				Portals[I].YTop = PortalHeight + rand() % (WindowHeight - 3 * PortalHeight);
				Portals[I].YBottom = Portals[I].YTop + PortalHeight;
				Portals[I].ScrolledAway = false;
			}

			Score = 0;
			PassedPortal = false;
			State = STATE_PLAYING;

			BackgroundTextureIndex = rand() % BackgroundTexturesCount;
			BirdTextureIndex = BirdAnimationFrames * (rand() % (BirdTexturesCount / BirdAnimationFrames));
			PipeTextureIndex = rand() % PipeTexturesCount;

			PlayAudio(&SwooshAudio);
		} break;
		
		case STATE_PLAYING: {
			if (SpacePressed) {
				PlayAudio(&WingAudio);

				BirdVelY = JumpVelocity;
			} else {
				BirdVelY += Gravitation;
			}
			BirdY += BirdVelY * DeltaTime;

			bool BirdBumped = false;
			bool BirdFellOff = false;

			if (BirdY > WindowHeight - ForegroundTexture.Height) {
				BirdFellOff = true;
			}

			bool PassedPortalPrevFrame = PassedPortal;
			PassedPortal = false;

			for (int I = 0; I < PortalsCount; I++) {
				Portals[I].X -= PortalsScrollSpeed;
				Portals[I].ScrolledAway = Portals[I].X + PipeTexture->Width < 0;
				if (Portals[I].ScrolledAway) {
					int RightmostX = 0;
					for (int J = 0; J < PortalsCount; J++) {
						if (RightmostX < Portals[J].X) {
							RightmostX = Portals[J].X;
						}
					}

					Portals[I].X = RightmostX + PortalsXMargin;
					Portals[I].YTop = PortalHeight + rand() % (WindowHeight - 3 * PortalHeight);
					Portals[I].YBottom = Portals[I].YTop + PortalHeight;
					Portals[I].ScrolledAway = false;
				}

				if (!Portals[I].ScrolledAway &&
				   (BirdX > Portals[I].X && BirdX + BirdTexture->Width < Portals[I].X + PipeTexture->Width) &&
				   (BirdY < Portals[I].YTop || BirdY + BirdTexture->Height > Portals[I].YBottom)) {
					BirdBumped = true;
				}

				if (!Portals[I].ScrolledAway && BirdX > Portals[I].X + PipeTexture->Width) {
					PassedPortal = true;
				}
			}

			BackgroundScroll = (BackgroundScroll - BackgroundScrollSpeed) % (BackgroundTexture->Width);

			if (BirdFellOff) {
				PlayAudio(&DieAudio);
			}

			if (BirdBumped) {
				PlayAudio(&HitAudio);
			}

			if (BirdFellOff || BirdBumped) {
				BackgroundScrollSpeed = 0;
				PortalsScrollSpeed = 0;

				State = STATE_DIED;
				goto outer;
			}

			if (PassedPortal && !PassedPortalPrevFrame) {
				PlayAudio(&PointAudio);

				Score++;
			}

			SDL_SetRenderDrawColor(Renderer, 255, 255, 255, 255);
			SDL_RenderClear(Renderer);

			RenderBackground(Renderer, BackgroundTexture, BackgroundScroll, WindowWidth, WindowHeight);
			RenderPipes(Renderer, PipeTexture, Portals, PortalsCount, WindowHeight);
			RenderForeground(Renderer, &ForegroundTexture, WindowWidth, WindowHeight);
			RenderBird(Renderer, BirdTexture, BirdX, BirdY, BirdVelY / 45.0f);
			RenderScore(Renderer, ScoreTextures, Score, ScoreX, ScoreY);

			SDL_RenderPresent(Renderer);
		} break;

		case STATE_DIED: {
			if (SpacePressed) {
				State = STATE_STARTING;
				goto outer;
			}

			BirdVelY += Gravitation;
			BirdY += BirdVelY * DeltaTime;

			SDL_SetRenderDrawColor(Renderer, 255, 255, 255, 255);
			SDL_RenderClear(Renderer);

			RenderBackground(Renderer, BackgroundTexture, BackgroundScroll, WindowWidth, WindowHeight);
			RenderPipes(Renderer, PipeTexture, Portals, PortalsCount, WindowHeight);
			RenderForeground(Renderer, &ForegroundTexture, WindowWidth, WindowHeight);
			RenderBird(Renderer, BirdTexture, BirdX, BirdY, 0.0f);
			RenderScore(Renderer, ScoreTextures, Score, ScoreX, ScoreY);

			SDL_Rect Source = {0, 0, GameoverTexture.Width, GameoverTexture.Height};
			SDL_Rect Destination = {WindowWidth / 2 - GameoverTexture.Width / 2,
				WindowHeight / 2 - GameoverTexture.Height / 2, Source.w, Source.h};
			SDL_RenderCopy(Renderer, GameoverTexture.Texture, &Source, &Destination);

			SDL_RenderPresent(Renderer);
		} break;
		}

		PrevFrameTicks = CurrFrameTicks;
		CurrFrameTicks = SDL_GetPerformanceCounter();
		DeltaTime = (CurrFrameTicks - PrevFrameTicks) / (float) SDL_GetPerformanceFrequency();

		if (DeltaTime < DesiredDelta) {
			SDL_Delay((DesiredDelta - DeltaTime) * 1000);
			DeltaTime = DesiredDelta;

			PrevFrameTicks = CurrFrameTicks;
			CurrFrameTicks = SDL_GetPerformanceCounter();
		}

		Time += DeltaTime;
		Frames += 1;

		if (Time >= 1.0f) {
			Time = 0.0f;
			Frames = 0;
		}
	}

	SDL_CloseAudioDevice(PointAudio.Device);
	SDL_CloseAudioDevice(WingAudio.Device);
	SDL_CloseAudioDevice(DieAudio.Device);
	SDL_CloseAudioDevice(HitAudio.Device);
	SDL_CloseAudioDevice(SwooshAudio.Device);

	// TODO: DestroyTexture
	SDL_DestroyRenderer(Renderer);
	SDL_DestroyWindow(Window);
	SDL_Quit();
	exit(0);
}
